package main

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSetupDB(t *testing.T) {

	Convey("Setup DB",t, func() {

		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}

		defer dbase.Close()
		dbmodel.ApplyDatabase()

	})
}
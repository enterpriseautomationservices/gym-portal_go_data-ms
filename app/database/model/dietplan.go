package dbmodel

type DietPlan struct {
	Model
	UserID        uint
	PTID          uint
	DietExclusion DietExclusion `gorm:"foreignkey:ID;association_foreignkey:DietExclusionID"`
}

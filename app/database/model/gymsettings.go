package dbmodel

// GymSettings Struct used to describe a gyms settings object in the database
type GymSettings struct {
	Model
	GymID uint
}

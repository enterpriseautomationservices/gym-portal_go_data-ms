package dbmodel

import (
	dbase "app/database"

	"github.com/jinzhu/gorm"
)

func ApplyDatabase() {
	dbase.Db.AutoMigrate(
		&LUDietExclusions{},
		&TrainingPlanExcercise{},
		&User{},
		&Media{},
		&Role{},
		&TrackerSession{},
		&TrainingPlan{},
		&UserCheckIn{},
		&IngredientExclusion{},
		&DietExclusion{},
		&DietPlan{},
		&Plan{},
		&Tag{},
		&Area{},
		&Speciality{},
		&Qualification{},
		&ClientFilter{},
		&Availability{},
		&Rating{},
		&Pt{},
		&Authorisation{},
	)

	userRoles := []string{"client", "personal_trainer", "gym_staff", "gym_owner"}

	for _, r := range userRoles {
		var role Role
		if err := dbase.Db.Where("name = ?", r).First(&role).Error; err != nil {
			// error handling...
			if gorm.IsRecordNotFoundError(err) {
				dbase.Db.Create(&Role{Name: r}) // newUser not user
			}
		}
	}

	return
}

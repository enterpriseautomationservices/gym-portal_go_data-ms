package dbmodel

type UserCheckIn struct {
	Model
	UserID uint
	Name   string
}

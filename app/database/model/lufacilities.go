package dbmodel

// LUFacilities Struct used to describe gym equipment items
type LUFacilities struct {
	Model
	Name  string
	Image []Media `gorm:"foreignkey:LUFacilitiesID"`
}

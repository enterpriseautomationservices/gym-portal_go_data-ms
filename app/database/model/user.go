package dbmodel

import (
	"time"
)

type User struct {
	Model
	Address
	Title                string                `json:"title"`
	FirstName            string                `json:"first_name"`
	LastName             string                `json:"last_name"`
	Gender               string                `json:"gender"`
	Email                string                `validate:"required,email" json:"email"`
	Password             string                `validate:"required" json:"password"`
	DoB                  *time.Time            `json:"date_of_birth"`
	Phone                string                `json:"phone"`
	Verification         string                `json:"verification"`
	Status               string                `json:"status"`
	DietExclusions       []DietExclusion       `gorm:"foreignkey:UserID" json:"diet_exclusions"`
	IngredientExclusions []IngredientExclusion `gorm:"foreignkey:UserID" json:"ingredient_exclusions"`
	UserCheckIns         []UserCheckIn         `gorm:"foreignkey:UserID" json:"user_checkins"`
	Roles                []Role                `gorm:"many2many:user_roles;" json:"roles"`
	Media                []Media               `gorm:"foreignkey:UserID" json:"media"`
	TrackerSessions      []TrackerSession      `gorm:"foreignkey:UserID" json:"tracker_sessions"`
	Plans                []Plan                `gorm:"foreignkey:UserID" json:"plans"`
}

package dbmodel

import "time"

type Area struct {
	Model
	Name         string         `json:"name"`
	PtID         uint           `json:"pt_id"`
	Postcode     string         `json:"postcode"`
	Longitude    float64        `json:"longitude"`
	Latitude     float64        `json:"latitude"`
	From         *time.Time     `json:"from"`
	To           *time.Time     `json:"to"`
	Availability []Availability `json:"availability" gorm:"foreignkey:AreaID"`
}

package dbmodel

type TrainingPlanExcercise struct {
	Model
	Reps         int
	Sets         int
	ExerciseType LUExercise
}

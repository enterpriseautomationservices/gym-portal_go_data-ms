package dbmodel

type Address struct {
	AddrLine1    string `json:"address_line_1"`
	AddrLine2    string `json:"address_line_2"`
	AddrLine3    string `json:"address_line_3"`
	AddrTown     string `json:"address_town"`
	AddrCounty   string `json:"address_county"`
	AddrPostCode string `json:"address_postcode"`
}

package dbmodel

type Speciality struct {
	Model
	Name string `json:"name"`
}

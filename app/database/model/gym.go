package dbmodel

// Gym Struct used to describe a gym object in the database
type Gym struct {
	Model
	Address
	OwnerID uint
	Owner   User `gorm:"foreignkey:OwnerID"`
	Name    string
	Phone   int
	status  string
	BlockPT Pt
}

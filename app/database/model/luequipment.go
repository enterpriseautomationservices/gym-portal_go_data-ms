package dbmodel

// LUEquipment Struct used to describe gym equipment items
type LUEquipment struct {
	Model
	Name  string
	Image []Media `gorm:"foreignkey:LUEquipmentID"`
}

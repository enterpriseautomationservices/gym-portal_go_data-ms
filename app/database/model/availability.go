package dbmodel

import (
	"time"
)

type Availability struct {
	Model
	Date   *time.Time `json:"date"`
	AreaID uint       `json:"area_id"`
}

package dbmodel

type Tag struct {
	Model
	OwnerID   int    `json:"owner_id"`
	OwnerType string `json:"owner_type"`
	Value     string `json:"rating"`
}

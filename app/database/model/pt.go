package dbmodel

type Pt struct {
	Model
	Address
	UserID         uint     `json:"user_id"`
	Name           string   `json:"name"`
	Phone          string   `json:"phone"`
	Status         string   `json:"status"`
	SortCode       string   `json:"sort_code"`
	AccountNumber  string   `json:"account_number"`
	Specialities   []Tag    `json:"specialities" gorm:"polymorphic:Owner;polymorphic_value:pt_specialities"`
	Qualifications []Tag    `json:"qualifications" gorm:"polymorphic:Owner;polymorphic_value:pt_qualifications"`
	ClientFilters  []Tag    `json:"client_filters" gorm:"polymorphic:Owner;polymorphic_value:pt_clientfilters"`
	BlockedUsers   []User   `json:"blocked_users" gorm:"many2many:pt_blocked_users;"`
	Ratings        []Rating `json:"ratings" gorm:"polymorphic:Owner;"`
	Areas          []Area   `json:"areas" gorm:"foreignkey:PtID;"`
}

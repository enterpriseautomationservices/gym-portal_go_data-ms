package dbmodel

type Rating struct {
	Model
	OwnerID   int    `json:"owner_id"`
	OwnerType string `json:"owner_type"`
	User      User   `json:"user" gorm:"foreignkey:UserID"`
	UserID    uint   `json:"user_id"`
	Rating    int    `json:"rating"`
}

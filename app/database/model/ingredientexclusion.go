package dbmodel

type IngredientExclusion struct {
	Model
	UserID                uint
	IngredientExclusionID uint
}

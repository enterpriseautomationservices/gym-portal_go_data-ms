package dbmodel

type DietExclusion struct {
	Model
	UserID          uint
	DietExclusionID uint
	DietExclusion   LUDietExclusions `gorm:"foreignkey:ID;association_foreignkey:DietExclusionID"`
}

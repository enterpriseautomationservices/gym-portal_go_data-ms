package dbmodel

type Role struct {
	Model
	Name string `sql:"not null; unique;" json:"name"`
}

package dbmodel

type Authorisation struct {
	Model
	Route  string
	Roles  string
	Filter string
	Args   string
}

package dbmodel

type Plan struct {
	Model
	UserID       uint
	PTID         uint
	TrainingPlan TrainingPlan
	DietPlan     DietPlan
}

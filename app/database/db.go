package dbase

import (
	"database/sql"
	"fmt"
	"os"

	
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var Db *gorm.DB

func Open() error {


	host := os.Getenv("MYSQL_HOST")
	username := os.Getenv("MYSQL_USERNAME")
	password := os.Getenv("MYSQL_PASSWORD")
	database := os.Getenv("MYSQL_DATABASE")

fmt.Println(host)

	var dberr error
	Db, dberr = gorm.Open("mysql", username+":"+password+"@tcp("+host+")/"+database+"?charset=utf8&parseTime=True&loc=Local")
	if dberr != nil {
		return dberr
	}
	return dberr
}

func Close() error {
	return Db.Close()
}

func DeleteCreatedEntities() func() {
	type entity struct {
		table   string
		keyname string
		key     interface{}
	}
	var entries []entity
	hookName := "cleanupHook"
	Db.Callback().Create().After("gorm:create").Register(hookName, func(scope *gorm.Scope) {
		fmt.Printf("Inserted entities of %s with %s=%v\n", scope.TableName(), scope.PrimaryKey(), scope.PrimaryKeyValue())
		entries = append(entries, entity{table: scope.TableName(), keyname: scope.PrimaryKey(), key: scope.PrimaryKeyValue()})
	})
	return func() {
		defer Db.Callback().Create().Remove(hookName)
		_, inTransaction := Db.CommonDB().(*sql.Tx)
		tx := Db
		if !inTransaction {
			tx = Db.Begin()
		}
		for i := len(entries) - 1; i >= 0; i-- {
			entry := entries[i]
			fmt.Printf("Deleting entities from '%s' table with key %v\n", entry.table, entry.key)
			tx.Table(entry.table).Where(entry.keyname+" = ?", entry.key).Delete("")
			if entry.table == "users" {
				fmt.Printf("    Deleting entities from 'user_roles' table with key user_id=%v\n", entry.key)
				tx.Table("user_roles").Where("user_id = ?", entry.key).Delete("")
			}
		}
		if !inTransaction {
			tx.Commit()
		}
	}
}

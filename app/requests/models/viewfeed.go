package reqmodel

// ViewFeed struct used to defined http requests from api routes
type ViewFeed struct {
	StatusText string
	StatusCode int
	Error      string
	Content    string
}

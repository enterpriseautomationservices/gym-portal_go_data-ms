package httputils

import (
	"encoding/json"
	"net/http"
)

func HandleResponse(w http.ResponseWriter, resp ResponseWrapper) {
	if resp.Error.StatusCode == 0 {
		js, err := json.Marshal(resp.Response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		w.Write(js)
	} else {
		w.WriteHeader(resp.Error.StatusCode)
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte(resp.Error.Message))
	}
}

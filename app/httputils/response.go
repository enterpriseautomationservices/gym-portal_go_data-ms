package httputils

type ResponseWrapper struct {
	Error    ResponseError
	Response interface{}
}

type ResponseError struct {
	StatusCode int
	Message    string
}

type ApplicationError struct {
	Error interface{} `json:"error"`
}

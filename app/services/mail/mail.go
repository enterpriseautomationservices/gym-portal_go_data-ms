package mail

import (
	"gopkg.in/gomail.v2"
)

func SendMail (toMail string, subject string, body string ) error {
	m := gomail.NewMessage()
	m.SetHeader("From", "rhys@enterpriseautomation.co.uk")
	m.SetHeader("To", toMail)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	d := gomail.NewDialer("smtp.gmail.com", 587, "rhys@enterpriseautomation.co.uk", "Hayley031207!")

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
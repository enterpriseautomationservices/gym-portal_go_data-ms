package auth

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"encoding/json"
	"net/http"
	"strings"
)

func User(r *http.Request) dbmodel.User {
	var user dbmodel.User
	json.NewDecoder(strings.NewReader(r.Context().Value("user").(string))).Decode(&user)
	dbase.Db.Set("gorm:auto_preload", true).Find(&user)
	return user
}

package auth

import (
	dbmodel "app/database/model"
	"log"
	"time"

	"github.com/go-redis/redis"
)

type Store interface {
	Get(string) (dbmodel.User, error)
	Set(string, dbmodel.User, time.Duration) error
	Del(string) error
}

type redisStore struct {
	client *redis.Client
}

func NewRedisStore() Store {
	client := redis.NewClient(&redis.Options{
		Addr:     "192.168.0.65:31000",
		Password: "",
		DB:       0,
	})

	_, err := client.Ping().Result()
	if err != nil {
		log.Fatalf("Failed to ping Redis: %v", err)
	}

	return &redisStore{
		client: client,
	}
}

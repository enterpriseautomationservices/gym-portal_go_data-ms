package auth

import (
	"context"
	"log"
	"app/httputils"
	"encoding/json"
	dbase "app/database"
	dbmodel "app/database/model"
	"net/http"
	"time"
)

var SessionsStore = NewRedisStore()

func Authorise(next http.Handler, routeRoles ...string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("pt-session")
		if err != nil {
			// response := httputils.ResponseWrapper{ Error: httputils.ResponseError{ StatusCode: 401, Message: "No valid Cookie in request header" } }
			// httputils.HandleResponse(w, response)
			http.Redirect(w, r, "/login", 302)
			return
		}
		sessionID := cookie.Value
		userData, err := SessionsStore.Get(sessionID)
		if err != nil {
			// response := httputils.ResponseWrapper{ Error: httputils.ResponseError{ StatusCode: 401, Message: "Session Expired/Invalid" } }
			// httputils.HandleResponse(w, response)
			http.Redirect(w, r, "/login", 302)
			return
		}
		expire := time.Now().AddDate(0, 0, 7)
		SessionsStore.Set(sessionID, userData, 7)
		newCookie := http.Cookie{Name: "pt-session", Path: "/", Value: sessionID, Expires: expire}
		http.SetCookie(w, &newCookie)
		marshalled, err := json.Marshal(userData)
		if err != nil {
			log.Println(`Error unmarshalling userData from Redis Session`)
			log.Println(err)
			return
		}
		var roles []dbmodel.Role
		dbase.Db.Model(&userData).Related(&roles, "Roles")
		for _, ro := range roles {
			for _, rro := range routeRoles {
				if ro.Name == rro {
					ctx := context.WithValue(r.Context(), "user", string(marshalled))
					next.ServeHTTP(w, r.WithContext(ctx))
					return
				}
			}
		}
		response := httputils.ResponseWrapper{ Error: httputils.ResponseError{ StatusCode: 403, Message: "You are not authorised to access this resource." } }
		httputils.HandleResponse(w, response)
	})
}
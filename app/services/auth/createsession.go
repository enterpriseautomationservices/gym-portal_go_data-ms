package auth

import (
	dbmodel "app/database/model"
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

func (r redisStore) Set(id string, session dbmodel.User, days time.Duration) error {
	bs, err := json.Marshal(session)
	if err != nil {
		return errors.Wrap(err, "failed to save session to redis")
	}
	ttlDays := 86400 * days
	if err := r.client.Set(id, bs, ttlDays*time.Second).Err(); err != nil {
		return errors.Wrap(err, "failed to save session to redis")
	}
	return nil
}

func (r redisStore) Get(id string) (dbmodel.User, error) {
	var session dbmodel.User

	bs, err := r.client.Get(id).Bytes()
	if err != nil {
		return session, errors.Wrap(err, "failed to get session from redis")
	}

	if err := json.Unmarshal(bs, &session); err != nil {
		return session, errors.Wrap(err, "failed to unmarshall session data")
	}

	return session, nil
}

func (r redisStore) Del(id string) error {
	r.client.Del(id)
	return nil
}

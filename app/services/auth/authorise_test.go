package auth

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/httputils"
	"bytes"
	"net/http"
	"net/http/httptest"
	"github.com/google/uuid"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestAuthorise(t *testing.T) {


	Convey("Subject: User authorisation on routes.", t, func() {
		// Initialise database connection for test suite
		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}
		defer dbase.Close()

		dbmodel.ApplyDatabase()

		cleaner := dbase.DeleteCreatedEntities()
		defer cleaner()



		Convey("Given that i am a Client", func() {
			var role dbmodel.Role
			dbase.Db.Where("name = ?", "client").First(&role)


			clientUser := dbmodel.User{
				FirstName: 	"Client",
				LastName:  	"User",
				Email:     	"client@ptp.com",
				Password:	"test",
				Status:		"verified",
				Roles:     	[]dbmodel.Role{role},
			}
			if err := dbase.Db.Create(&clientUser).Error; err != nil {
				panic(err)
			}

			sessionID := uuid.New()
			SessionsStore.Set(sessionID.String(), clientUser, 7)

			Convey("When accessing a client resource", func() {
				

				req, err := http.NewRequest("POST", "/api/client/signup", bytes.NewBuffer([]byte("")))
				if err != nil {
					t.Fatal(err)
				}

				req.AddCookie(&http.Cookie{Name: "pt-session", Value: sessionID.String()})
				rr := httptest.NewRecorder()
				handler := Authorise(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					response := httputils.ResponseWrapper{ Response: map[string]interface{}{ } }
					httputils.HandleResponse(w, response)
				}), "client")
				handler.ServeHTTP(rr, req)

				Convey("The request should succeed", func() {
					result := rr.Code
					So(result, ShouldEqual, 200)

				})
			})

			Convey("When accessing a NON client resource", func() {
				

				req, err := http.NewRequest("POST", "/api/public/signup", bytes.NewBuffer([]byte("")))
				if err != nil {
					t.Fatal(err)
				}

				req.AddCookie(&http.Cookie{Name: "pt-session", Value: sessionID.String()})
				rr := httptest.NewRecorder()
				handler := Authorise(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					response := httputils.ResponseWrapper{ Response: map[string]interface{}{ } }
					httputils.HandleResponse(w, response)
				}), "personal_trainer")
				handler.ServeHTTP(rr, req)

				Convey("The request should fail", func() {
					result := rr.Code
					So(result, ShouldEqual, 403)

				})
			})
		})
	})
}
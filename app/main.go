package main

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/routes/client"
	"app/routes/pt"
	"app/routes/public"
	"app/services/auth"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

const PORT = 80

func main() {

	SetupDB()

	defer dbase.Close()

	r := mux.NewRouter()

	// public routes
	r.Handle("/api/register", http.HandlerFunc(public.Signup))
	r.Handle("/api/verify", http.HandlerFunc(public.Verify))
	r.Handle("/api/login", http.HandlerFunc(public.Login))

	// authed routes
	r.Handle("/api/becomePt", auth.Authorise(http.HandlerFunc(client.BecomePt), "client"))
	r.Handle("/api/feed", auth.Authorise(http.HandlerFunc(client.Feed), "client"))
	r.Handle("/api/me", auth.Authorise(http.HandlerFunc(client.Me), "client"))
	r.Handle("/api/logout", auth.Authorise(http.HandlerFunc(client.Logout), "client"))
	r.Handle("/api/profile", auth.Authorise(http.HandlerFunc(client.Profile), "client"))
	r.Handle("/api/addressLookup", auth.Authorise(http.HandlerFunc(client.AddressLookup), "client"))

	// r.Handle("/api/pt/profile", http.HandlerFunc(pt.Profile))
	r.Handle("/api/pt/profile", auth.Authorise(http.HandlerFunc(pt.Profile), "personal_trainer"))

	http.Handle("/", r)

	log.Println(fmt.Sprintf("Started on port %d", PORT))
	err := http.ListenAndServe(fmt.Sprintf(":%d", PORT), r)
	if err != nil {
		log.Println(err)
	}

}

func SetupDB() {
	if openerr := dbase.Open(); openerr != nil {
		println(openerr)
	}

	dbmodel.ApplyDatabase()
}

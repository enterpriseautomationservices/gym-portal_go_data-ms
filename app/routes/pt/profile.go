package pt

import (
	dbase "app/database"
	dbmodel "app/database/model"

	"app/httputils"
	"encoding/json"
	"net/http"
	"strings"
)

type requestBody struct {
	Pt dbmodel.Pt `json:"pt"`
}

func Profile(w http.ResponseWriter, r *http.Request) {

	//the user making the call
	var storedUserData dbmodel.User
	json.NewDecoder(strings.NewReader(r.Context().Value("user").(string))).Decode(&storedUserData)

	switch r.Method {
	// fetch pt table for (user)
	case http.MethodGet:
		var pt dbmodel.Pt
		if err := dbase.Db.Set("gorm:auto_preload", true).Model(&storedUserData).Related(&pt).Error; err != nil {
			response := httputils.ResponseWrapper{Response: map[string]interface{}{"error": "Failed to get profile."}}
			httputils.HandleResponse(w, response)
			return
		}
		response := httputils.ResponseWrapper{Response: map[string]interface{}{"pt": pt}}
		httputils.HandleResponse(w, response)
	case http.MethodPost:
		http.NotFound(w, r)
		return

	// update pt table for (user)
	case http.MethodPut:

		// var pt = (user)s related pt table entry
		var pt dbmodel.Pt
		if err := dbase.Db.Set("gorm:auto_preload", true).Model(&storedUserData).Related(&pt).Error; err != nil {
			response := httputils.ResponseWrapper{Response: map[string]interface{}{"error": "Failed to get profile."}}
			httputils.HandleResponse(w, response)
			return
		}

		// var req = (user)s request body (pt) struct
		var req requestBody
		reqDecoder := json.NewDecoder(r.Body)
		reqDecoder.Decode(&req)

		// override the pt entrysprimary key (id) and users foreignkey (user_id)
		req.Pt.ID = pt.ID
		req.Pt.UserID = pt.UserID

		// update table (pts id = pt.ID) with data (req.pt.*)
		if err := dbase.Db.Model(&pt).Updates(req.Pt).Error; err != nil {
			response := httputils.ResponseWrapper{Response: map[string]interface{}{"error": "Failed to update profile."}}
			httputils.HandleResponse(w, response)
			return
		}
		response := httputils.ResponseWrapper{Response: map[string]interface{}{"message": "Successfully updated profile."}}
		httputils.HandleResponse(w, response)
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}
}

package pt

import (
	dbase "app/database"
	dbmodel "app/database/model"

	"bytes"
	"net/http"
	"net/http/httptest"
	"github.com/google/uuid"
	"testing"
	"encoding/json"
	"app/services/auth"
	. "github.com/smartystreets/goconvey/convey"
)


type getResponseBody struct {
	Pt dbmodel.Pt `json:"pt"`
}
type putResponseBody struct {
	Message string `json:"message"`
}

func TestAuthorise(t *testing.T) {


	Convey("Subject: PT profile route.", t, func() {
		// Initialise database connection for test suite
		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}

		defer dbase.Close()

		dbmodel.ApplyDatabase()

		cleaner := dbase.DeleteCreatedEntities()
		defer cleaner()

		Convey("Given that i am a Pt", func() {

			var role dbmodel.Role
			dbase.Db.Where("name = ?", "personal_trainer").First(&role)
			user := dbmodel.User{
				FirstName: 	"test",
				LastName:  	"test",
				Email:     	"test@ptp.com",
				Password:	"test",
				Status:		"verified",
				Roles:     	[]dbmodel.Role{role},
			}
			if err := dbase.Db.Create(&user).Error; err != nil {
				panic(err)
			}
			pt := dbmodel.Pt{
				UserID: user.ID,
				Name: user.FirstName,
			}
			if err := dbase.Db.Create(&pt).Error; err != nil {
				panic(err)
			}

			sessionID := uuid.New()
			auth.SessionsStore.Set(sessionID.String(), user, 7)

			
			Convey("When requesting my profile", func() {

				req, err := http.NewRequest("GET", "", bytes.NewBuffer([]byte("")))
				if err != nil {
					t.Fatal(err)
				}

				req.AddCookie(&http.Cookie{Name: "pt-session", Value: sessionID.String()})
				rr := httptest.NewRecorder()
				handler := auth.Authorise(http.HandlerFunc(Profile), "personal_trainer")
				handler.ServeHTTP(rr, req)

				Convey("The request should return a profile struct", func() {
					var req getResponseBody
					reqDecoder := json.NewDecoder(rr.Body)
					reqDecoder.Decode(&req)
					So(req.Pt.Name, ShouldEqual, "test")
				})
			})

			Convey("When updating my profile", func() {

				rb := getResponseBody{
					Pt: dbmodel.Pt{
						Name: "newValue",
					},
				}
				reqBodyBytes := new(bytes.Buffer)
				json.NewEncoder(reqBodyBytes).Encode(rb)

				
				req, err := http.NewRequest("PUT", "", bytes.NewBuffer(reqBodyBytes.Bytes()))
				if err != nil {
					t.Fatal(err)
				}

				req.AddCookie(&http.Cookie{Name: "pt-session", Value: sessionID.String()})
				rr := httptest.NewRecorder()
				handler := auth.Authorise(http.HandlerFunc(Profile), "personal_trainer")
				handler.ServeHTTP(rr, req)

				Convey("The request should succeed", func() {
					var req putResponseBody
					reqDecoder := json.NewDecoder(rr.Body)
					reqDecoder.Decode(&req)
					So(req.Message, ShouldEqual, "Successfully updated profile.")
				})
				Convey("The request should update my profile", func() {

					var pt dbmodel.Pt
					dbase.Db.Model(&user).Related(&pt)
					So(pt.Name, ShouldEqual, "newValue")
				})
			})

			
		})
	})
}
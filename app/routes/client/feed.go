package client

import (
	"app/services/auth"
	"encoding/json"
	"fmt"
	"net/http"
)

func Feed(w http.ResponseWriter, r *http.Request) {
	user := auth.User(r)
	fmt.Println(user)

	feed := map[string]interface{}{
		"items": []interface{}{},
	}

	// feed["items"] = append(feed["items"], ...stuff)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(feed)
}

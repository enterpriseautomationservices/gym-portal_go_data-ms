package client

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/services/auth"
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBecomePt(t *testing.T) {

	Convey("Given that i am a Client", t, func() {

		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}

		defer dbase.Close()
		dbmodel.ApplyDatabase()
		cleaner := dbase.DeleteCreatedEntities()
		defer cleaner()
		var role dbmodel.Role

		dbase.Db.Where("name = ?", "client").First(&role)
		clientUser := dbmodel.User{
			FirstName: "Client",
			LastName:  "User",
			Email:     "client@ptp.com",
			Password:  "test",
			Status:    "verified",
			Roles:     []dbmodel.Role{role},
			Address: dbmodel.Address{
				AddrLine1:    "string",
				AddrLine2:    "string",
				AddrLine3:    "string",
				AddrTown:     "string",
				AddrCounty:   "string",
				AddrPostCode: "string",
			}}
		dbase.Db.Create(&clientUser)

		sessionID := uuid.New()
		auth.SessionsStore.Set(sessionID.String(), clientUser, 7)

		Convey("When I request to become a pt", func() {
			req, err := http.NewRequest("POST", "", bytes.NewBuffer([]byte("")))
			if err != nil {
				t.Fatal(err)
			}
			req.AddCookie(&http.Cookie{Name: "pt-session", Value: sessionID.String()})
			rr := httptest.NewRecorder()
			handler := auth.Authorise(http.HandlerFunc(BecomePt), "client")
			handler.ServeHTTP(rr, req)
			Convey("The request should succeed", func() {
				result := rr.Code
				So(result, ShouldEqual, 200)
			})

			Convey("The user should have a pt role", func() {
				var roles []dbmodel.Role
				dbase.Db.Model(&clientUser).Related(&roles, "Roles")
				var roleCompare = ""
				var roleToCheck = "personal_trainer"
				for i := 0; i < len(roles); i++ {
					var roleName = roles[i].Name
					if roleName == roleToCheck {
						roleCompare = roleName
					}
				}
				So(roleCompare, ShouldEqual, roleToCheck)
				//dbase.Db.Delete(&roles)
			})
			Convey("The user should have a pt profile", func() {
				var pt dbmodel.Pt
				ptNotFound := dbase.Db.Where("user_id = ?", clientUser.ID).First(&pt).RecordNotFound()
				fmt.Println(clientUser.ID)
				So(ptNotFound, ShouldBeFalse)
				//	dbase.Db.Delete(&pt)
				//dbase.Db.Delete(&user)
			})
		})

	})
}

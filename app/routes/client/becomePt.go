package client

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/httputils"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

// become a pt, adds pt role to user if it does not exist and creates entry in pt table with the equivilent user data
func BecomePt(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		http.NotFound(w, r)
		return
	case http.MethodPost:
		var storedUserData dbmodel.User
		json.NewDecoder(strings.NewReader(r.Context().Value("user").(string))).Decode(&storedUserData)
		var user dbmodel.User
		dbase.Db.Preload("Roles").Where("ID = ?", storedUserData.ID).First(&user)
		var roles []dbmodel.Role
		dbase.Db.Model(&user).Related(&roles, "Roles")
		var hasPt = false
		for _, v := range user.Roles {
			if v.Name == "personal_trainer" {
				hasPt = true
			}
		}
		if hasPt == false {
			var role dbmodel.Role
			dbase.Db.Where("name = ?", "personal_trainer").First(&role)
			user.Roles = append(user.Roles, role)
			dbase.Db.Save(&user)
			var ptData dbmodel.Pt
			ptData.Name = user.FirstName + " " + user.LastName
			ptData.Phone = user.Phone
			ptData.Address = user.Address
			ptData.Status = "inactive"
			ptData.UserID = user.ID

			if err := dbase.Db.Create(&ptData).Error; err != nil {
				fmt.Println(err)
				response := httputils.ResponseWrapper{Response: map[string]interface{}{"error": err}}
				httputils.HandleResponse(w, response)
				return
			}
			response := httputils.ResponseWrapper{Response: map[string]interface{}{"pt": ptData}}
			httputils.HandleResponse(w, response)
			return
		}
		response := httputils.ResponseWrapper{Response: map[string]interface{}{"error": "You already have Pt role"}}
		httputils.HandleResponse(w, response)
		return
	case http.MethodPut:
		http.NotFound(w, r)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}
}

package client

import (
	"app/httputils"
	"app/services/auth"
	"log"
	"net/http"
)

// Logout Function to allow users to delete session from redis and destroy their session cookie
func Logout(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		http.NotFound(w, r)
		return
	case http.MethodPost:
		sessionID, err := r.Cookie("pt-session")
		if err != nil {
			log.Println("err")
		}
		auth.SessionsStore.Del(sessionID.Value)
		response := httputils.ResponseWrapper{Response: map[string]interface{}{"message": "logged out successfully"}}
		httputils.HandleResponse(w, response)
		return
	case http.MethodPut:
		http.NotFound(w, r)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}

}

package client

import (
	dbase "app/database"
	dbmodel "app/database/model"

	"app/httputils"
	"encoding/json"
	"net/http"
	"strings"
)

// Logout Function to allow users to delete session from redis and destroy their session cookie
func Me(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:

		var storedUserData dbmodel.User
		json.NewDecoder(strings.NewReader(r.Context().Value("user").(string))).Decode(&storedUserData)

		var user dbmodel.User
		var roles []dbmodel.Role
		dbase.Db.Set("gorm:auto_preload", true).Model(&storedUserData).Find(&user).Related(&roles, "Roles")
		// dbase.Db.Model(&user)
		user.Roles = roles

		response := httputils.ResponseWrapper{Response: map[string]interface{}{"user": user}}
		httputils.HandleResponse(w, response)
		return
	case http.MethodPost:
		http.NotFound(w, r)
		return
	case http.MethodPut:
		http.NotFound(w, r)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}

}

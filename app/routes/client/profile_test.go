package client

import (
	dbase "app/database"
	dbmodel "app/database/model"

	"fmt"
	"bytes"
	"net/http"
	"net/http/httptest"
	"github.com/google/uuid"
	"testing"
	"encoding/json"
	"app/services/auth"
	. "github.com/smartystreets/goconvey/convey"
)

type getProfileBody struct {
	User dbmodel.User `json:"user"`
}

type putResponseBody struct {
	Message string `json:"message"`
}

func TestUserProfile(t *testing.T) {

	Convey("Route user profile", t, func() {
		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}
		defer dbase.Close()
		dbmodel.ApplyDatabase()
		cleaner := dbase.DeleteCreatedEntities()
		defer cleaner()

		Convey("Given that i am a User", func() {
			var role dbmodel.Role
			dbase.Db.Where("name = ?", "client").First(&role)
			user := dbmodel.User{
				FirstName: 	"test",
				LastName:  	"test",
				Email:     	"test@ptp.com",
				Password:	"test",
				Status:		"verified",
				Roles:     	[]dbmodel.Role{role},
			}
			if err := dbase.Db.Create(&user).Error; err != nil {
				panic(err)
			}

			sessionID := uuid.New()
			auth.SessionsStore.Set(sessionID.String(), user, 7)

			Convey("When requesting my profile", func() {
				req, err := http.NewRequest("GET", "", bytes.NewBuffer([]byte("")))
				if err != nil {
					t.Fatal(err)
				}
				req.AddCookie(&http.Cookie{Name: "pt-session", Value: sessionID.String()})
				rr := httptest.NewRecorder()
				handler := auth.Authorise(http.HandlerFunc(Profile), "client")
				handler.ServeHTTP(rr, req)

				Convey("The request should return a profile struct", func() {
					var req getProfileBody
					reqDecoder := json.NewDecoder(rr.Body)
					fmt.Println(rr.Body.String())
					reqDecoder.Decode(&req)
					So(req.User.FirstName, ShouldEqual, "test")
				})
			})

			Convey("When updating my profile", func() {

				rb := getProfileBody{
					User: dbmodel.User{
						FirstName: "newValue",
					},
				}
				reqBodyBytes := new(bytes.Buffer)
				json.NewEncoder(reqBodyBytes).Encode(rb)

				req, err := http.NewRequest("PUT", "", bytes.NewBuffer(reqBodyBytes.Bytes()))
				if err != nil {
					t.Fatal(err)
				}

				req.AddCookie(&http.Cookie{Name: "pt-session", Value: sessionID.String()})
				rr := httptest.NewRecorder()
				handler := auth.Authorise(http.HandlerFunc(Profile), "client")
				handler.ServeHTTP(rr, req)

				Convey("The request should succeed", func() {
					var req putResponseBody
					reqDecoder := json.NewDecoder(rr.Body)
					reqDecoder.Decode(&req)
					So(req.Message, ShouldEqual, "Successfully updated profile.")
				})
				Convey("The request should update my profile", func() {

					dbase.Db.Find(&user)
					So(user.FirstName, ShouldEqual, "newValue")
				})
			})
		})
	})
}
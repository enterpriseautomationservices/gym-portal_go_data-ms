package client

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/httputils"
	"encoding/json"
	"net/http"
	"strings"
)

type userProfileBody struct {
	User dbmodel.User `json:"user"`
}

func Profile(w http.ResponseWriter, r *http.Request) {
	var storedUserData dbmodel.User
	json.NewDecoder(strings.NewReader(r.Context().Value("user").(string))).Decode(&storedUserData)
	switch r.Method {
	case http.MethodGet:
		var user dbmodel.User
		dbase.Db.Set("gorm:auto_preload", true).Model(&storedUserData).Find(&user)
		response := httputils.ResponseWrapper{Response: map[string]interface{}{"user": user}}
		httputils.HandleResponse(w, response)
	case http.MethodPost:
		http.NotFound(w, r)
		return

	case http.MethodPut:
		var user dbmodel.User
		dbase.Db.Set("gorm:auto_preload", true).Model(&storedUserData).Find(&user)

		var req userProfileBody
		reqDecoder := json.NewDecoder(r.Body)
		reqDecoder.Decode(&req)

		req.User.Email = user.Email
		req.User.Password = user.Password
		req.User.Verification = user.Verification
		req.User.Status = user.Status
		req.User.ID = user.ID
		req.User.Roles = user.Roles

		if err := dbase.Db.Model(&storedUserData).Updates(req.User).Error; err != nil {
			response := httputils.ResponseWrapper{Response: map[string]interface{}{"error": "Failed to update profile."}}
			httputils.HandleResponse(w, response)
			return
		}
		response := httputils.ResponseWrapper{Response: map[string]interface{}{"message": "Successfully updated profile."}}
		httputils.HandleResponse(w, response)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		return
	}
}

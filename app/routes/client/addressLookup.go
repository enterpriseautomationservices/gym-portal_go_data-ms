package client

import (
	"encoding/json"
	// "io/ioutil"
	"app/httputils"
	"fmt"
	"net/http"
	"net/url"
)

type requestBody struct {
	Postcode   string `json:"postcode"`
	Identifier string `json:"identifier"`
}

type Addresses struct {
	Postcode  string    `json:"postcode"`
	Latitude  float64   `json:"latitude"`
	Longitude float64   `json:"longitude"`
	Addresses []Address `json:"addresses"`
}

type Address struct {
	FormattedAddress  []string `json:"formatted_address"`
	Thoroughfare      string   `json:"thoroughfare"`
	BuildingName      string   `json:"building_name"`
	SubBuildingName   string   `json:"sub_building_name"`
	SubBuildingNumber string   `json:"sub_building_number"`
	BuildingNumber    string   `json:"building_number"`
	Line1             string   `json:"line_1"`
	Line2             string   `json:"line_2"`
	Line3             string   `json:"line_3"`
	Line4             string   `json:"line_4"`
	Locality          string   `json:"locality"`
	TownOrCity        string   `json:"town_or_city"`
	County            string   `json:"county"`
	District          string   `json:"district"`
	Country           string   `json:"country"`
}

// address lookup api to return a list of addresses
func AddressLookup(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		http.NotFound(w, r)
		return
	case http.MethodPost:
		// parse req.body
		var req requestBody
		reqDecoder := json.NewDecoder(r.Body)
		reqDecoder.Decode(&req)

		// build api url
		baseUrl, err := url.Parse("https://api.getAddress.io")
		if err != nil {
			response := httputils.ResponseWrapper{Error: httputils.ResponseError{StatusCode: 500, Message: "internal server error"}}
			httputils.HandleResponse(w, response)
			return
		}
		baseUrl.Path += fmt.Sprintf("/find/%s/%s", req.Postcode, req.Identifier)
		params := url.Values{}
		params.Add("expand", "true")
		params.Add("api-key", "Sc25UslcOUGWfQrTRqhg9w22074")
		baseUrl.RawQuery = params.Encode()

		// call address lookup api
		resp, err := http.Get(baseUrl.String())
		if err != nil {
			response := httputils.ResponseWrapper{Error: httputils.ResponseError{StatusCode: 500, Message: "internal server error"}}
			httputils.HandleResponse(w, response)
			return
		}
		defer resp.Body.Close()

		// parse response
		var addresses Addresses
		resDecoder := json.NewDecoder(resp.Body)
		resDecoder.Decode(&addresses)
		if addresses.Postcode == "" {
			response := httputils.ResponseWrapper{Response: httputils.ApplicationError{Error: map[string]interface{}{"message": "No addresses found."}}}
			httputils.HandleResponse(w, response)
			return
		}

		// send response
		response := httputils.ResponseWrapper{Response: addresses}
		httputils.HandleResponse(w, response)
		return
	case http.MethodPut:
		http.NotFound(w, r)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}

}

package client

import (
	dbmodel "app/database/model"
	"app/services/auth"
	"bytes"

	// "encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLogout(t *testing.T) {

	Convey("Describe: Logout for a user with a valid session", t, func() {

		Convey("When a user with a valid session attempts to logout", func() {
			sessionID, err := uuid.NewUUID()
			if err != nil {
				log.Println(err)
			}
			expire := time.Now().AddDate(0, 0, 7)
			var User dbmodel.User
			auth.SessionsStore.Set(sessionID.String(), User, 7)

			cookie := http.Cookie{Name: "pt-session", Path: "/", Value: sessionID.String(), Expires: expire}
			req, err := http.NewRequest("POST", "/api/client/logout", bytes.NewBuffer([]byte(`hello`)))
			if err != nil {
				t.Fatal(err)
			}

			req.AddCookie(&cookie)
			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(Logout)
			handler.ServeHTTP(rr, req)

			Convey("The logout should succeed", func() {
				expected := `{"message":"logged out successfully"}`
				result := rr.Body.String()
				So(result, ShouldEqual, expected)
			})

			Convey("The server should read the pt-session cookie", func() {

			})

			Convey("The server should expire the redis session TTL", func() {

			})

			Convey("The server should return a 200 status code", func() {
				expected := 200
				result := rr.Code
				So(result, ShouldEqual, expected)
			})

		})

		Convey("When a user without a valid session attempts to logout", func() {

			Convey("The logout should fail", func() {

			})

		})

	})

}

package public

import (
	dbase "app/database"
	dbmodel "app/database/model"
	// "app/httputils"
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"io/ioutil"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"golang.org/x/crypto/bcrypt"
)

func TestLogin(t *testing.T) {
	Convey("Subject: Log User into App", t, func() {

		// Initialise database connection for test suite
		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}
		defer dbase.Close()

		dbmodel.ApplyDatabase()

		cleaner := dbase.DeleteCreatedEntities()
		defer cleaner()

		Convey("Given a registered user", func() {

			hash, err := bcrypt.GenerateFromPassword([]byte(`password`), bcrypt.MinCost)
			if err != nil {
				log.Println(err)
			}
			correctPassword := &dbmodel.User{Email: "email@gmail.com", Status: "verified", Password: string(hash)}
			dbase.Db.Create(&correctPassword)

			unverifiedUser := &dbmodel.User{Email: "email1@gmail.com", Status: "disabled", Password: string(hash)}
			dbase.Db.Create(&unverifiedUser)

			correctDetails := []byte(`{"Email":"email@gmail.com","Password":"password"}`)
			unverifiedDetails := []byte(`{"Email":"email1@gmail.com","Password":"password"}`)
			incorrectPassword := []byte(`{"Email":"email@gmail.com","Password":"wrongpassword"}`)
			userDoesNotExist := []byte(`{"Email":"fake@gmail.com","Password":"fakepassword"}`)
			//badDataDetails:=[]byte(`"Emaile":"fake@gmail.com","Password""fakepassword"}`)
			Convey("When logging in with correct details", func() {
				req, err := http.NewRequest("POST", "/api/public/login", bytes.NewBuffer(correctDetails))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Login)
				handler.ServeHTTP(rr, req)
				
				Convey("The login should succeed", func() {
					readBuf, _ := ioutil.ReadAll(rr.Body)
					var loginUser dbmodel.User
					json.Unmarshal(readBuf, &loginUser)

					expected := `email@gmail.com`
					result := loginUser.Email
					So(result, ShouldEqual, expected)
				})
				Convey("The server should return a 200 status code", func() {
					expected := 200
					result := rr.Code
					So(result, ShouldEqual, expected)
				})
				Convey("The server should return the pt-session cookie", func() {
					//log.Println(rr.Cookie("pt-session"))
				})
				Convey("The redis server sessionID should match the pt-session cookie", func() {
	
				})
			})
			Convey("When logging in with incorrect details", func() {
				req, err := http.NewRequest("POST", "/api/public/login", bytes.NewBuffer(incorrectPassword))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Login)
				handler.ServeHTTP(rr, req)
			
				Convey("The Returned value should be Incorrect Password", func() {
					expected := `{"error":{"message":"Incorrect Username or Password"}}`
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})
			})
			Convey("When logging in with an unverified account", func() {
				req, err := http.NewRequest("POST", "/api/public/login", bytes.NewBuffer(unverifiedDetails))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Login)
				handler.ServeHTTP(rr, req)

				Convey("Returned value should be Unverified", func() {
					expected := `{"error":{"message":"Email not verified"}}`
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})
				
			})
			Convey("When user does not exist", func() {
				req, err := http.NewRequest("POST", "/api/public/login", bytes.NewBuffer(userDoesNotExist))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Login)
				handler.ServeHTTP(rr, req)
				Convey("The Returned value should be Unverified", func() {
					expected := `{"error":{"message":"User not found."}}`
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})
			})
			
		})
	})
}

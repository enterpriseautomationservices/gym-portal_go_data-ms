package public

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/httputils"

	// "app/services/mail"
	"encoding/json"
	"log"
	"net/http"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/go-playground/validator.v9"
)

type resp struct {
	Message string `json:"message"`
}

// Signup Function to allow users to sign up
func Signup(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		http.NotFound(w, r)
		return
	case http.MethodPost:
		decoder := json.NewDecoder(r.Body)
		var userData dbmodel.User
		decoder.Decode(&userData)
		//validate Struct Data
		var validate *validator.Validate
		validate = validator.New()
		var validationError error
		validationError = validate.Struct(userData)
		if validationError != nil {
			// response := httputils.Response{StatusText: "SIGNUP_VALIDATION_ERROR", StatusCode: 500, Error: string(validationError.Error()), Content: ""}
			response := httputils.ResponseWrapper{Response: httputils.ApplicationError{map[string]interface{}{"message": string(validationError.Error())}}}
			httputils.HandleResponse(w, response)
			return
		}

		//check if user exists
		var User dbmodel.User
		if dbase.Db.Where("email = ?", userData.Email).First(&User).RecordNotFound() {
			uuid := uuid.New()
			userData.Status = "disabled"
			userData.Verification = uuid.String()
			var role dbmodel.Role
			dbase.Db.Where("name = ?", "client").First(&role)
			//encrypt password
			password := []byte(userData.Password)
			hash, err := bcrypt.GenerateFromPassword(password, bcrypt.MinCost)
			if err != nil {
				log.Println(err)
			}
			userData.Password = string(hash)
			userData.Roles = []dbmodel.Role{role}

			dbase.Db.Create(&userData)
			// if err := mail.SendMail(userData.Email, "Please verify your e-mail address", "Please visit url: https://blah/"+userData.Verification); err != nil {
			// 	userData.Status = "email_failed"
			// 	dbase.Db.Save(&userData)
			// }
			// response := httputils.Response{StatusText: "SIGNUP_SUCCESS", StatusCode: 201, Error: "", Content: "Signup Success"}
			response := httputils.ResponseWrapper{Response: map[string]interface{}{"verification": userData.Verification}}

			httputils.HandleResponse(w, response)
		} else {
			if User.Status == "disabled" {
				// response := httputils.Response{StatusText: "SIGNUP_USER_EXISTS_NOT_VERIFIED", StatusCode: 409, Error: "User Already Exists but needs verifying", Content: ""}
				response := httputils.ResponseWrapper{Response: httputils.ApplicationError{map[string]interface{}{"message": "User Already Exists but needs verifying"}}}
				httputils.HandleResponse(w, response)
			} else {
				// response := httputils.Response{StatusText: "SIGNUP_USER_EXISTS", StatusCode: 409, Error: "User Already Exists", Content: ""}
				response := httputils.ResponseWrapper{Response: httputils.ApplicationError{map[string]interface{}{"message": "User Already Exists"}}}
				httputils.HandleResponse(w, response)
			}
		}

		return
	case http.MethodPut:
		http.NotFound(w, r)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}

}

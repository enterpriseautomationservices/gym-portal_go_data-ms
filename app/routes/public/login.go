package public

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/httputils"
	"app/services/auth"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

// Login Function to enable users to login, create a session and set a cookie
func Login(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		http.NotFound(w, r)
		return
	case http.MethodPost:
		decoder := json.NewDecoder(r.Body)
		var userData dbmodel.User
		decoder.Decode(&userData)

		var User dbmodel.User

		fmt.Println(userData.Email)
		if dbase.Db.Where("email = ?", userData.Email).First(&User).RecordNotFound() {
			response := httputils.ResponseWrapper{Response: httputils.ApplicationError{map[string]interface{}{"message": "User not found."}}}
			// response := httputils.Response{StatusText: "NO_USER", StatusCode: 500, Error: "User Does Not Exist", Content: ""}
			httputils.HandleResponse(w, response)
		} else {
			if User.Status == "verified" {
				bytePassword := []byte(userData.Password)
				if comparePasswords(User.Password, bytePassword) {
					sessionID := uuid.New()

					expire := time.Now().AddDate(0, 0, 7)
					auth.SessionsStore.Set(sessionID.String(), User, 7)
					cookie := http.Cookie{Name: "pt-session", Path: "/", Value: sessionID.String(), Expires: expire}
					http.SetCookie(w, &cookie)

					response := httputils.ResponseWrapper{Response: User}
					// response := httputils.Response{StatusText: "LOGIN_SUCCESS", StatusCode: 200, Error: "", Content: ""}
					httputils.HandleResponse(w, response)
				} else {
					response := httputils.ResponseWrapper{Response: httputils.ApplicationError{map[string]interface{}{"message": "Incorrect Username or Password"}}}
					// response := httputils.Response{StatusText: "INCORRECT_PASSWORD", StatusCode: 401, Error: "Incorrect Password", Content: ""}
					httputils.HandleResponse(w, response)
				}
			} else {
				response := httputils.ResponseWrapper{Response: httputils.ApplicationError{map[string]interface{}{"message": "Email not verified"}}}
				// response := httputils.Response{StatusText: "NOT_VERIFIED", StatusCode: 500, Error: "Not Verified", Content: ""}
				httputils.HandleResponse(w, response)
			}
		}
		return
	case http.MethodPut:
		http.NotFound(w, r)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}

}

func comparePasswords(hashedPwd string, plainPwd []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		//log.Println(err)
		return false
	}
	return true
}

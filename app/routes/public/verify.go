package public

import (
	dbase "app/database"
	dbmodel "app/database/model"
	"app/httputils"
	"encoding/json"
	"fmt"
	"net/http"
)

// Verify Function used to ensure a user has provided a valid email address
func Verify(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		http.NotFound(w, r)
		return
	case http.MethodPost:
		decoder := json.NewDecoder(r.Body)
		var userData dbmodel.User
		decoder.Decode(&userData)

		fmt.Println(userData.Verification)

		var User dbmodel.User
		if dbase.Db.Where("verification = ?", userData.Verification).First(&User).RecordNotFound() {
			// response := httputils.Response{StatusText: `VERIFICATION_FAILED`, StatusCode: 403, Error: `Invalid verification code.`, Content: `Invalid verification code.`}
			response := httputils.ResponseWrapper{Response: httputils.ApplicationError{map[string]interface{}{"message": "Invalid verification code"}}}
			httputils.HandleResponse(w, response)
		} else {
			User.Status = "verified"
			dbase.Db.Save(&User)
			// response := httputils.Response{StatusText: `SUCCESS`, StatusCode: 200, Error: ``, Content: `User: ` + User.Email + ` has been successfully verified.`}
			response := httputils.ResponseWrapper{Response: map[string]interface{}{"message": "user verified"}}
			httputils.HandleResponse(w, response)
		}
		return
	case http.MethodPut:
		http.NotFound(w, r)
		return
	case http.MethodDelete:
		http.NotFound(w, r)
		return
	default:
		http.NotFound(w, r)
		return
	}

}

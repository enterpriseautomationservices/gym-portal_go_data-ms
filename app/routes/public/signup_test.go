package public

import (
	dbase "app/database"
	dbmodel "app/database/model"

	// "app/httputils"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSignUp(t *testing.T) {

	Convey("Subject: User Sign-Up Process", t, func() {

		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}
		defer dbase.Close()

		dbmodel.ApplyDatabase()

		cleaner := dbase.DeleteCreatedEntities()
		defer cleaner()

		Convey("Given a user has submitted a sign-up form", func() {

			Convey("When trying to sign up without a complete data-set", func() {
				incompleteuser := dbmodel.User{FirstName: "Test", LastName: "User", Email: "test.user@ptportal.com"}
				incompleteJSON, err := json.Marshal(incompleteuser)
				if err != nil {
					t.Fatal(err)
					return
				}

				incompleteDetails := []byte(incompleteJSON)

				req, err := http.NewRequest("POST", "/api/public/signup", bytes.NewBuffer(incompleteDetails))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Signup)
				handler.ServeHTTP(rr, req)

				Convey("The sign-up should fail", func() {
					expected := `{"error":{"message":"Key: 'User.Password' Error:Field validation for 'Password' failed on the 'required' tag"}}`
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})

			})

			Convey("When trying to sign up with an existing user", func() {

				// Set up pre-existing user to cause e-mail address clash
				addexistinguser := dbmodel.User{Email: "test.user@ptportal.com"}
				dbase.Db.Create(&addexistinguser)

				existinguser := dbmodel.User{Title: "Mr", FirstName: "Test", LastName: "User", Email: "test.user@ptportal.com", Password: "Password123", Address: dbmodel.Address{AddrLine1: "1 My Street", AddrTown: "My Town", AddrPostCode: "MY10 0ME"}}
				existingJSON, err := json.Marshal(existinguser)
				if err != nil {
					t.Fatal(err)
					return
				}

				existingDetails := []byte(existingJSON)

				req, err := http.NewRequest("POST", "/api/public/signup", bytes.NewBuffer(existingDetails))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Signup)
				handler.ServeHTTP(rr, req)

				Convey("The sign-up should fail", func() {
					expected := `{"error":{"message":"User Already Exists"}}`
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})

			})

			Convey("When trying to sign up with an existing user that is un-verified", func() {

				Convey("The sign-up should fail", func() {

					// Set up pre-existing user to cause e-mail address clash
					addexistingunverifieduser := dbmodel.User{Email: "test@ptp.com", Status: "disabled"}
					dbase.Db.Create(&addexistingunverifieduser)

					existingunverifieduser := dbmodel.User{Title: "Mr", FirstName: "Test", LastName: "User", Email: "test@ptp.com", Password: "Password123", Address: dbmodel.Address{AddrLine1: "1 My Street", AddrTown: "My Town", AddrPostCode: "MY10 0ME"}}
					existingunverifiedJSON, err := json.Marshal(existingunverifieduser)
					if err != nil {
						t.Fatal(err)
						return
					}

					existingunverifiedDetails := []byte(existingunverifiedJSON)

					req, err := http.NewRequest("POST", "/api/public/signup", bytes.NewBuffer(existingunverifiedDetails))
					if err != nil {
						t.Fatal(err)
					}
					rr := httptest.NewRecorder()
					handler := http.HandlerFunc(Signup)
					handler.ServeHTTP(rr, req)

					Convey("The sign-up should fail", func() {
						expected := `{"error":{"message":"User Already Exists but needs verifying"}}`
						result := rr.Body.String()
						So(result, ShouldEqual, expected)
					})
				})

			})

			Convey("When trying to sign up with correct data", func() {

				validuser := dbmodel.User{Title: "Mr", FirstName: "Test", LastName: "User", Email: "valid.user@ptportal.com", Password: "Password123", Address: dbmodel.Address{AddrLine1: "1 My Street", AddrTown: "My Town", AddrPostCode: "MY10 0ME"}}
				validuserJSON, err := json.Marshal(validuser)
				if err != nil {
					t.Fatal(err)
					return
				}

				validuserDetails := []byte(validuserJSON)

				req, err := http.NewRequest("POST", "/api/public/signup", bytes.NewBuffer(validuserDetails))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Signup)
				handler.ServeHTTP(rr, req)

				var passwordUser dbmodel.User
				dbase.Db.Where("email = ?", validuser.Email).First(&passwordUser)

				Convey("The sign-up should succeed", func() {
					expected := fmt.Sprintf(`{"verification":"%s"}`, passwordUser.Verification)
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})

				Convey("The application should send an email", func() {

				})

				Convey("The user should have a status of \"disabled\"", func() {
					expected := "disabled"
					result := passwordUser.Status
					So(result, ShouldEqual, expected)
				})

				Convey("The password should be salted and hashed", func() {
					expected := validuser.Password
					result := passwordUser.Password
					So(result, ShouldNotEqual, expected)
				})

			})

		})

	})

}

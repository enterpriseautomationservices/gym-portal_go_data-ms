package public

import (
	dbase "app/database"
	dbmodel "app/database/model"

	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestVerify(t *testing.T) {

	Convey("Subject: User verification after sign-up", t, func() {

		if openerr := dbase.Open(); openerr != nil {
			println(openerr)
		}
		defer dbase.Close()

		dbmodel.ApplyDatabase()

		cleaner := dbase.DeleteCreatedEntities()
		defer cleaner()

		Convey("Given a user has signed up successfully", func() {
			//Generate uuid's for testing
			
			correctUUID, err := uuid.NewUUID()
			if err != nil {
				t.Fatal(err)
			}
			incorrectUUID, err := uuid.NewUUID()
			if err != nil {
				t.Fatal(err)
			}

			//Insert a sample user into the database
			correctuser := &dbmodel.User{Verification: correctUUID.String(), Status: "disabled"}
			incorrectuser := &dbmodel.User{Verification: incorrectUUID.String(), Status: "disabled"}
			dbase.Db.Create(&correctuser)

			Convey("When calling verification with an invalid uuid", func() {

				incorrectJSON, err := json.Marshal(incorrectuser)
				if err != nil {
					t.Fatal(err)
					return
				}

				incorrectDetails := []byte(incorrectJSON)

				req, err := http.NewRequest("POST", "/api/public/verify", bytes.NewBuffer(incorrectDetails))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Verify)
				handler.ServeHTTP(rr, req)


				Convey("The verification should fail", func() {
					expected := `{"error":{"message":"Invalid verification code"}}`
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})

			})

			Convey("When calling verification with a valid uuid", func() {

				correctJSON, err := json.Marshal(correctuser)
				if err != nil {
					t.Fatal(err)
					return
				}

				correctDetails := []byte(correctJSON)

				req, err := http.NewRequest("POST", "/api/public/verify", bytes.NewBuffer(correctDetails))
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				handler := http.HandlerFunc(Verify)
				handler.ServeHTTP(rr, req)


				Convey("The verification should succeed", func() {
					expected := `{"message":"user verified"}`
					result := rr.Body.String()
					So(result, ShouldEqual, expected)
				})

				Convey("The user status should be activated", func() {
					var User dbmodel.User
					dbase.Db.Where("verification = ?", correctuser.Verification).First(&User)
					expected := `verified`
					result := User.Status
					So(result, ShouldEqual, expected)
				})

			})

		})

	})

}

FROM golang:alpine AS build

RUN apk update && apk add --no-cache git openssh

# RUN adduser -D -g '' gouser

WORKDIR $GOPATH/src/app

COPY ./app .

ADD ~/.ssh/id_rsa id_rsa ~/.ssh/id_rsa

ADD ~/.ssh/known_hosts ~/.ssh/known_hosts

RUN git config --global url.git@bitbucket.org:.insteadOf https://bitbucket.org/

RUN go get github.com/Masterminds/glide 

RUN glide install

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -a -installsuffix cgo -o /go/bin/app .

FROM scratch

COPY --from=build /etc/passwd /etc/passwd

COPY --from=build /go/bin/app /go/bin/app



# USER gouser

ENTRYPOINT ["/go/bin/app"]